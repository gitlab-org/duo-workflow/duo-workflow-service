from typing import Type
from unittest.mock import AsyncMock, MagicMock, patch

import pytest
from pydantic import BaseModel, ValidationError

from duo_workflow_service.tools.duo_base_tool import DuoBaseTool


class DummyTool(DuoBaseTool):
    name: str = "Dummy tool"
    description: str = "Create a new tool"
    args_schema: Type[BaseModel] = BaseModel  # type: ignore


def test_gitlab_client():
    tool = DummyTool(metadata={})
    with pytest.raises(RuntimeError):
        tool.gitlab_client

    client = MagicMock()
    tool = DummyTool(metadata={"gitlab_client": client})
    assert tool.gitlab_client == client
