import json
from unittest.mock import AsyncMock

import pytest

from duo_workflow_service.tools.issue import (
    CreateIssue,
    CreateIssueInput,
    CreateIssueNote,
    CreateIssueNoteInput,
    GetIssue,
    GetIssueInput,
    GetIssueNote,
    GetIssueNoteInput,
    ListIssueNotes,
    ListIssueNotesInput,
    ListIssues,
    ListIssuesInput,
    UpdateIssue,
    UpdateIssueInput,
)


@pytest.mark.asyncio
async def test_create_issue():
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.apost.return_value = {
        "id": 1,
        "title": "Test Issue",
        "description": "This is a test issue",
    }
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = CreateIssue(description="created issue description", metadata=metadata)

    response = await tool._arun(
        project_id=1,
        title="Test Issue",
        description="This is a test issue",
        labels="bug,urgent",
        assignee_ids=[10, 11],
        milestone_id=5,
        due_date="2023-12-31",
    )

    expected_response = json.dumps(
        {
            "created_issue": {
                "id": 1,
                "title": "Test Issue",
                "description": "This is a test issue",
            }
        }
    )
    assert response == expected_response

    gitlab_client_mock.apost.assert_called_once_with(
        path="/api/v4/projects/1/issues",
        body=json.dumps(
            {
                "title": "Test Issue",
                "description": "This is a test issue",
                "labels": "bug,urgent",
                "assignee_ids": [10, 11],
                "milestone_id": 5,
                "due_date": "2023-12-31",
            }
        ),
    )


def test_create_issue_format_display_message():
    tool = CreateIssue(description="Create issue description")

    input_data = CreateIssueInput(
        project_id=123,
        title="New Bug Report",
        description="This is a test issue",
        labels=None,
        assignee_ids=None,
        confidential=None,
        due_date=None,
        issue_type=None,
    )

    message = tool.format_display_message(input_data)

    expected_message = "Create issue 'New Bug Report' in project 123"
    assert message == expected_message


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "state,labels,milestone,scope,search,expected_params",
    [
        (None, None, None, None, None, {}),
        (
            "opened",
            "bug",
            "v1.0",
            "all",
            "important",
            {
                "state": "opened",
                "labels": "bug",
                "milestone": "v1.0",
                "scope": "all",
                "search": "important",
            },
        ),
    ],
)
async def test_list_issues(state, labels, milestone, scope, search, expected_params):
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.aget.return_value = [
        {"id": 1, "title": "Issue 1"},
        {"id": 2, "title": "Issue 2"},
    ]
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = ListIssues(description="listed issue description", metadata=metadata)

    response = await tool._arun(
        project_id=1,
        state=state,
        labels=labels,
        milestone=milestone,
        scope=scope,
        search=search,
    )

    expected_response = json.dumps(
        {"issues": [{"id": 1, "title": "Issue 1"}, {"id": 2, "title": "Issue 2"}]}
    )
    assert response == expected_response

    gitlab_client_mock.aget.assert_called_once_with(
        path="/api/v4/projects/1/issues", params=expected_params, parse_json=False
    )


def test_list_issues_format_display_message():
    tool = ListIssues(description="List issues description")

    input_data = ListIssuesInput(
        project_id=123,
        assignee_id=None,
        assignee_username=None,
        author_id=None,
        author_username=None,
        confidential=None,
        created_after=None,
        created_before=None,
        due_date=None,
        health_status=None,
        issue_type=None,
        labels=None,
        scope=None,
        search=None,
        sort=None,
        state=None,
    )

    message = tool.format_display_message(input_data)

    expected_message = "List issues in project 123"
    assert message == expected_message


@pytest.mark.asyncio
async def test_get_issue():
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.aget.return_value = {
        "id": 1,
        "title": "Test Issue",
        "description": "This is a test issue",
    }
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = GetIssue(description="get issue description", metadata=metadata)

    response = await tool._arun(project_id=1, issue_id=123)

    expected_response = json.dumps(
        {
            "issue": {
                "id": 1,
                "title": "Test Issue",
                "description": "This is a test issue",
            }
        }
    )
    assert response == expected_response

    gitlab_client_mock.aget.assert_called_once_with(
        path="/api/v4/projects/1/issues/123", parse_json=False
    )


def test_get_issue_format_display_message():
    tool = GetIssue(description="Get issue description")

    input_data = GetIssueInput(project_id=123, issue_id=456)

    message = tool.format_display_message(input_data)

    expected_message = "Read issue #456 in project 123"
    assert message == expected_message


@pytest.mark.asyncio
async def test_update_issue():
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.aput.return_value = {
        "id": 123,
        "title": "Updated Test Issue",
        "description": "This is an updated test issue",
        "labels": ["bug", "critical"],
        "assignee_ids": [15, 16],
        "state": "closed",
    }
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = UpdateIssue(description="update issue description", metadata=metadata)

    response = await tool._arun(
        project_id=1,
        issue_id=123,
        title="Updated Test Issue",
        description="This is an updated test issue",
        labels="bug,critical",
        assignee_ids=[15, 16],
        state_event="close",
    )

    expected_response = json.dumps(
        {
            "updated_issue": {
                "id": 123,
                "title": "Updated Test Issue",
                "description": "This is an updated test issue",
                "labels": ["bug", "critical"],
                "assignee_ids": [15, 16],
                "state": "closed",
            }
        }
    )
    assert response == expected_response

    gitlab_client_mock.aput.assert_called_once_with(
        path="/api/v4/projects/1/issues/123",
        body=json.dumps(
            {
                "title": "Updated Test Issue",
                "description": "This is an updated test issue",
                "labels": "bug,critical",
                "assignee_ids": [15, 16],
                "state_event": "close",
            }
        ),
    )


def test_update_issue_format_display_message():
    tool = UpdateIssue(description="Update issue description")

    input_data = UpdateIssueInput(
        project_id=123,
        issue_id=456,
        title=None,
        description=None,
        labels=None,
        assignee_ids=None,
        confidential=None,
        due_date=None,
        state_event=None,
        discussion_locked=None,
    )

    message = tool.format_display_message(input_data)

    expected_message = "Update issue #456 in project 123"
    assert message == expected_message


@pytest.mark.asyncio
async def test_create_issue_note():
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.apost.return_value = {
        "id": 1,
        "body": "Test note",
        "created_at": "2024-01-01T12:00:00Z",
        "author": {"id": 1, "name": "Test User"},
    }
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = CreateIssueNote(
        description="create issue note description", metadata=metadata
    )

    response = await tool._arun(project_id=1, issue_id=123, body="Test note")

    expected_response = json.dumps(
        {
            "status": "success",
            "body": "Test note",
            "response": {
                "id": 1,
                "body": "Test note",
                "created_at": "2024-01-01T12:00:00Z",
                "author": {"id": 1, "name": "Test User"},
            },
        }
    )
    assert response == expected_response

    gitlab_client_mock.apost.assert_called_once_with(
        path="/api/v4/projects/1/issues/123/notes",
        body=json.dumps({"body": "Test note"}),
    )


def test_create_issue_note_format_display_message():
    tool = CreateIssueNote(description="Create issue note description")

    input_data = CreateIssueNoteInput(
        project_id=123, issue_id=456, body="This is a comment"
    )

    message = tool.format_display_message(input_data)

    expected_message = "Add comment to issue #456 in project 123"
    assert message == expected_message


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "sort,order_by,expected_params",
    [
        (None, None, {}),
        ("asc", None, {"sort": "asc"}),
        ("desc", "created_at", {"sort": "desc", "order_by": "created_at"}),
        (None, "updated_at", {"order_by": "updated_at"}),
    ],
)
async def test_list_issue_notes(sort, order_by, expected_params):
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.aget.return_value = [
        {"id": 1, "body": "Note 1"},
        {"id": 2, "body": "Note 2"},
    ]
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = ListIssueNotes(description="list issue notes description", metadata=metadata)

    response = await tool._arun(
        project_id=1, issue_id=123, sort=sort, order_by=order_by
    )

    expected_response = json.dumps(
        {"notes": [{"id": 1, "body": "Note 1"}, {"id": 2, "body": "Note 2"}]}
    )
    assert response == expected_response

    gitlab_client_mock.aget.assert_called_once_with(
        path="/api/v4/projects/1/issues/123/notes",
        params=expected_params,
        parse_json=False,
    )


def test_list_issue_notes_format_display_message():
    tool = ListIssueNotes(description="List issue notes description")

    input_data = ListIssueNotesInput(
        project_id=123, issue_id=456, sort=None, order_by=None
    )

    message = tool.format_display_message(input_data)

    expected_message = "Read comments on issue #456 in project 123"
    assert message == expected_message


@pytest.mark.asyncio
async def test_get_issue_note():
    gitlab_client_mock = AsyncMock()
    gitlab_client_mock.aget.return_value = {
        "id": 1,
        "body": "Test note",
        "created_at": "2024-01-01T12:00:00Z",
        "author": {"id": 1, "name": "Test User"},
    }
    metadata = {
        "gitlab_client": gitlab_client_mock,
    }

    tool = GetIssueNote(description="get issue note description", metadata=metadata)

    response = await tool._arun(project_id=1, issue_id=123, note_id=1)

    expected_response = json.dumps(
        {
            "note": {
                "id": 1,
                "body": "Test note",
                "created_at": "2024-01-01T12:00:00Z",
                "author": {"id": 1, "name": "Test User"},
            }
        }
    )
    assert response == expected_response

    gitlab_client_mock.aget.assert_called_once_with(
        path="/api/v4/projects/1/issues/123/notes/1", parse_json=False
    )


def test_get_issue_note_format_display_message():
    tool = GetIssueNote(description="Get issue note description")

    input_data = GetIssueNoteInput(project_id=123, issue_id=456, note_id=789)

    message = tool.format_display_message(input_data)

    expected_message = "Read comment #789 on issue #456 in project 123"
    assert message == expected_message
