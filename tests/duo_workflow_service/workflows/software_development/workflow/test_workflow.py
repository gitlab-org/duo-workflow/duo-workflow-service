import asyncio
import os
from unittest.mock import AsyncMock, MagicMock, call, patch
from uuid import uuid4

import pytest
from langchain_core.messages import AIMessage, HumanMessage, SystemMessage
from langgraph.checkpoint.base import CheckpointTuple
from langgraph.checkpoint.memory import MemorySaver

from contract import contract_pb2
from duo_workflow_service.components import ToolsRegistry
from duo_workflow_service.entities import Plan, WorkflowStatusEnum
from duo_workflow_service.workflows.software_development import Workflow


@pytest.fixture
def checkpoint_tuple():
    return CheckpointTuple(
        config={"configurable": {"thread_id": "123", "checkpoint_id": str(uuid4())}},
        checkpoint={
            "channel_values": {"status": WorkflowStatusEnum.NOT_STARTED},
            "id": str(uuid4()),
            "channel_versions": {},
            "pending_sends": [],
            "versions_seen": {},
            "ts": "",
            "v": 0,
        },
        metadata={"step": 0},
        parent_config={"configurable": {"thread_id": "123", "checkpoint_id": None}},
    )


@pytest.mark.asyncio
async def test_workflow_initialization():
    workflow = Workflow("123", {})
    assert isinstance(workflow._outbox, asyncio.Queue)
    assert isinstance(workflow._inbox, asyncio.Queue)


def _agent_responses(status: WorkflowStatusEnum, agent_name: str):
    return [
        {
            "plan": Plan(steps=[]),
            "status": status,
            "conversation_history": {
                agent_name: [
                    SystemMessage(content="system message"),
                    HumanMessage(content="human message"),
                    AIMessage(
                        content="No tool calls in last AI message, route to the supervisor",
                    ),
                ],
            },
        },
        {
            "plan": Plan(steps=[]),
            "status": status,
            "conversation_history": {
                agent_name: [
                    SystemMessage(content="system message"),
                    HumanMessage(content="human message"),
                    AIMessage(
                        content="Tool calls are present, route to planner tools execution",
                        tool_calls=[
                            {
                                "id": "1",
                                "name": "update_plan",
                                "args": {"summary": "done"},
                            }
                        ],
                    ),
                ],
            },
        },
        {
            "plan": Plan(steps=[]),
            "status": status,
            "conversation_history": {
                agent_name: [
                    SystemMessage(content="system message"),
                    HumanMessage(content="human message"),
                    AIMessage(
                        content="HandoverTool call is present, route to the next agent",
                        tool_calls=[
                            {
                                "id": "1",
                                "name": "handover_tool",
                                "args": {"summary": "done"},
                            }
                        ],
                    ),
                ],
            },
        },
    ]


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch("duo_workflow_service.workflows.software_development.workflow.new_chat_client")
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run(
    mock_goal_disambiguator_component,
    mock_gitlab_workflow,
    mock_chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry,
    checkpoint_tuple,
):
    mock_tools_registry.configure = AsyncMock(
        return_value=MagicMock(spec=ToolsRegistry)
    )
    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=None)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )
    mock_git_lab_workflow_instance.get_next_version = MagicMock(return_value=1)

    mock_tools_executor.return_value.run.side_effect = [
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.PLANNING,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.PLANNING,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.EXECUTION,
            "conversation_history": {},
        },
    ]

    mock_handover_agent.return_value.run.side_effect = [
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.COMPLETED,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.COMPLETED,
            "conversation_history": {},
        },
    ]

    mock_agent.return_value.run.side_effect = [
        *_agent_responses(
            WorkflowStatusEnum.PLANNING, "context_builder"
        ),  # context builder responses
        *_agent_responses(WorkflowStatusEnum.PLANNING, "planner"),  # planner responses
        *_agent_responses(
            WorkflowStatusEnum.EXECUTION, "executor"
        ),  # executor responses
    ]

    mock_plan_supervisor_agent.return_value.run.return_value = {
        "plan": Plan(steps=[]),
        "status": WorkflowStatusEnum.EXECUTION,
        "conversation_history": {},
    }

    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"

    workflow = Workflow("123", {})
    await workflow.run("test_goal")

    assert mock_goal_disambiguator_component.return_value.attach.call_count == 1

    assert mock_agent.call_count == 3
    assert mock_agent.return_value.run.call_count >= 5

    assert mock_tools_executor.call_count == 3
    assert mock_tools_executor.return_value.run.call_count >= 1

    assert mock_handover_agent.call_count == 3
    assert mock_handover_agent.return_value.run.call_count >= 1

    assert mock_plan_supervisor_agent.call_count == 3
    assert mock_plan_supervisor_agent.return_value.run.call_count >= 2

    assert mock_git_lab_workflow_instance.aput.call_count >= 1
    assert mock_git_lab_workflow_instance.aget_tuple.call_count >= 1

    assert workflow.is_done


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch("duo_workflow_service.workflows.software_development.workflow.new_chat_client")
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run_with_memory_saver(
    mock_goal_disambiguator_component,
    mock_gitlab_workflow,
    mock_chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry,
    checkpoint_tuple,
):

    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry.configure = AsyncMock(
        return_value=MagicMock(spec=ToolsRegistry)
    )
    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = MemorySaver()
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = True
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=None)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )

    mock_tools_executor.return_value.run.side_effect = [
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.PLANNING,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.PLANNING,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.EXECUTION,
            "conversation_history": {},
        },
    ]

    mock_handover_agent.return_value.run.side_effect = [
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.COMPLETED,
            "conversation_history": {},
        },
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.COMPLETED,
            "conversation_history": {},
        },
    ]

    mock_agent.return_value.run.side_effect = [
        *_agent_responses(
            WorkflowStatusEnum.PLANNING, "context_builder"
        ),  # context builder responses
        *_agent_responses(WorkflowStatusEnum.PLANNING, "planner"),  # planner responses
        *_agent_responses(
            WorkflowStatusEnum.EXECUTION, "executor"
        ),  # executor responses
    ]

    mock_plan_supervisor_agent.return_value.run.return_value = {
        "plan": Plan(steps=[]),
        "status": WorkflowStatusEnum.EXECUTION,
        "conversation_history": {},
    }

    workflow = Workflow("123", {})
    await workflow.run("test_goal")

    assert mock_agent.call_count == 3
    assert mock_agent.return_value.run.call_count >= 5

    assert mock_tools_executor.call_count == 3
    assert mock_tools_executor.return_value.run.call_count >= 1

    assert mock_handover_agent.call_count == 3
    assert mock_handover_agent.return_value.run.call_count >= 1

    assert mock_plan_supervisor_agent.call_count == 3
    assert mock_plan_supervisor_agent.return_value.run.call_count >= 2

    assert mock_git_lab_workflow_instance.aput.call_count == 0
    assert mock_git_lab_workflow_instance.aget_tuple.call_count == 0

    assert workflow.is_done


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch("duo_workflow_service.workflows.software_development.workflow.new_chat_client")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run_when_exception(
    mock_goal_disambiguator_component,
    chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_gitlab_workflow,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry,
):
    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry.configure = AsyncMock(
        return_value=MagicMock(spec=ToolsRegistry)
    )

    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=None)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )
    mock_git_lab_workflow_instance.get_next_version = MagicMock(return_value=1)

    class AsyncIterator:
        def __init__(self):
            pass

        def __aiter__(self):
            return self

        def __anext__(self):
            raise asyncio.CancelledError()

    workflow = Workflow("123", {})
    with patch(
        "duo_workflow_service.workflows.software_development.workflow.StateGraph"
    ) as graph:
        compiled_graph = MagicMock()
        compiled_graph.aget_state = AsyncMock(return_value=None)
        compiled_graph.astream.return_value = AsyncIterator()
        instance = graph.return_value
        instance.compile.return_value = compiled_graph
        await workflow.run("test_goal")

    assert workflow.is_done


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.new_chat_client",
    autospec=True,
)
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run_with_error_state(
    mock_goal_disambiguator_component,
    mock_gitlab_workflow,
    mock_chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry,
    checkpoint_tuple,
):
    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry.configure = AsyncMock(
        return_value=MagicMock(spec=ToolsRegistry)
    )

    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=None)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )
    mock_git_lab_workflow_instance.get_next_version = MagicMock(return_value=1)

    mock_tools_executor.return_value.run.side_effect = [
        {
            "plan": Plan(steps=[]),
            "status": WorkflowStatusEnum.ERROR,
            "conversation_history": {},
        }
    ]

    mock_agent.return_value.run.side_effect = [
        *_agent_responses(WorkflowStatusEnum.PLANNING, "context_builder")
    ]

    mock_plan_supervisor_agent.return_value.run.return_value = {
        "plan": Plan(steps=[]),
        "status": WorkflowStatusEnum.EXECUTION,
        "conversation_history": {},
    }

    workflow = Workflow("123", {})

    await workflow.run("test_goal")

    assert mock_agent.call_count == 3
    assert mock_agent.return_value.run.call_count == 2

    assert mock_tools_executor.call_count == 3
    assert mock_tools_executor.return_value.run.call_count == 1

    assert workflow.is_done


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry")
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch("duo_workflow_service.workflows.software_development.workflow.new_chat_client")
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
async def test_workflow_run_with_tools_registry(
    mock_goal_disambiguator_component,
    mock_gitlab_workflow,
    chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry_cls,
    checkpoint_tuple,
):
    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry = MagicMock(spec=ToolsRegistry)
    mock_tools_registry_cls.configure = AsyncMock(return_value=mock_tools_registry)
    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=checkpoint_tuple)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[checkpoint_tuple])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )
    mock_git_lab_workflow_instance.get_next_version = MagicMock(return_value=1)

    class AsyncIterator:
        def __init__(self):
            pass

        def __aiter__(self):
            return self

        def __anext__(self):
            raise asyncio.CancelledError()

    workflow = Workflow("123", {})
    with patch(
        "duo_workflow_service.workflows.software_development.workflow.StateGraph"
    ) as graph_cls:
        compiled_graph = MagicMock()
        compiled_graph.astream.return_value = AsyncIterator()
        instance = graph_cls.return_value
        instance.compile.return_value = compiled_graph
        await workflow.run("test_goal")

    executor_tools = [
        "create_issue",
        "list_issues",
        "get_issue",
        "update_issue",
        "create_issue_note",
        "create_merge_request_note",
        "list_issue_notes",
        "get_issue_note",
        "create_merge_request",
        "get_job_logs",
        "get_merge_request",
        "get_pipeline_errors",
        "get_project",
        "run_read_only_git_command",
        "run_git_command",
        "list_all_merge_request_notes",
        "list_merge_request_diffs",
        "gitlab_issue_search",
        "gitlab_merge_request_search",
        "run_command",
        "read_file",
        "ls_files",
        "create_file_with_contents",
        "edit_file",
        "find_files",
        "grep_files",
        "mkdir",
        "add_new_task",
        "remove_task",
        "update_task_description",
        "get_plan",
        "set_task_status",
        "handover_tool",
        "get_epic",
        "list_epics",
        "create_epic",
        "update_epic",
    ]

    context_builder_tools = [
        "list_issues",
        "get_issue",
        "list_issue_notes",
        "get_issue_note",
        "get_job_logs",
        "get_merge_request",
        "get_project",
        "get_pipeline_errors",
        "run_read_only_git_command",
        "run_git_command",
        "list_all_merge_request_notes",
        "list_merge_request_diffs",
        "gitlab_issue_search",
        "gitlab_merge_request_search",
        "read_file",
        "ls_files",
        "find_files",
        "grep_files",
        "run_command",
        "handover_tool",
        "get_epic",
        "list_epics",
    ]

    mock_tools_registry.get_batch.assert_has_calls(
        [
            call(executor_tools),
            call(context_builder_tools),
        ],
        any_order=True,
    )
    mock_tools_registry.get_handlers.assert_has_calls(
        [
            call(executor_tools),
            call(executor_tools),
            call(context_builder_tools),
        ],
        any_order=True,
    )
    mock_tools_registry.get.assert_has_calls(
        [
            call("get_plan"),
            call("add_new_task"),
            call("remove_task"),
            call("update_task_description"),
            call("handover_tool"),
        ],
        any_order=True,
    )

    # Verify get_plan is called once in executor setup and once in planner setup
    assert mock_tools_registry.get.call_args_list.count(call("get_plan")) == 2


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run_with_setup_error(
    mock_goal_disambiguator_component,
    mock_fetch_project_data_with_workflow_id,
    mock_gitlab_workflow,
    mock_tools_registry,
):
    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry.configure = AsyncMock(
        side_effect=Exception("Failed to configure tools")
    )

    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=checkpoint_tuple)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[checkpoint_tuple])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )
    mock_git_lab_workflow_instance.get_next_version = MagicMock(return_value=1)

    workflow = Workflow("123", {})
    await workflow.run("test_goal")

    assert workflow.is_done


@pytest.mark.asyncio
@patch("duo_workflow_service.workflows.abstract_workflow.ToolsRegistry", autospec=True)
@patch("duo_workflow_service.workflows.software_development.workflow.Agent")
@patch("duo_workflow_service.workflows.software_development.workflow.HandoverAgent")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.PlanSupervisorAgent"
)
@patch("duo_workflow_service.workflows.software_development.workflow.ToolsExecutor")
@patch("duo_workflow_service.workflows.abstract_workflow.GitLabWorkflow", autospec=True)
@patch(
    "duo_workflow_service.workflows.abstract_workflow.fetch_project_data_with_workflow_id"
)
@patch("duo_workflow_service.workflows.software_development.workflow.new_chat_client")
@patch(
    "duo_workflow_service.workflows.software_development.workflow.GoalDisambiguationComponent",
    autospec=True,
)
@patch.dict(os.environ, {"DW_INTERNAL_EVENT__ENABLED": "true"})
async def test_workflow_run_with_retry(
    mock_goal_disambiguator_component,
    chat_client,
    mock_fetch_project_data_with_workflow_id,
    mock_gitlab_workflow,
    mock_tools_executor,
    mock_plan_supervisor_agent,
    mock_handover_agent,
    mock_agent,
    mock_tools_registry,
    checkpoint_tuple,
):
    mock_goal_disambiguator_component.return_value.attach.return_value = "planning"
    mock_tools_registry.configure = AsyncMock(
        return_value=MagicMock(spec=ToolsRegistry)
    )

    mock_fetch_project_data_with_workflow_id = AsyncMock(
        return_value={
            "id": 1,
            "name": "test-project",
            "description": "This is a test project",
            "http_url_to_repo": "https://example.com/project",
        }
    )

    mock_git_lab_workflow_instance = mock_gitlab_workflow.return_value
    mock_git_lab_workflow_instance.__aenter__.return_value = (
        mock_git_lab_workflow_instance
    )
    mock_git_lab_workflow_instance.__aexit__.return_value = None
    mock_git_lab_workflow_instance._offline_mode = False
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(return_value=None)
    mock_git_lab_workflow_instance.alist = AsyncMock(return_value=[])
    mock_git_lab_workflow_instance.aput = AsyncMock(
        return_value={
            "configurable": {"thread_id": "123", "checkpoint_id": "checkpoint1"}
        }
    )

    # Setup AsyncIterator for workflow steps
    class AsyncIterator:
        def __init__(self):
            self.call_count = 0

        def __aiter__(self):
            return self

        async def __anext__(self):
            self.call_count += 1
            if self.call_count > 3:
                raise StopAsyncIteration
            if self.call_count == 1:
                raise asyncio.CancelledError()
            return {"build_context": {}}

    workflow = Workflow("123", {})
    async_iterator = AsyncIterator()

    with patch(
        "duo_workflow_service.workflows.software_development.workflow.StateGraph"
    ) as graph:
        compiled_graph = MagicMock()
        compiled_graph.astream.return_value = async_iterator
        compiled_graph.aget_state = AsyncMock(return_value=None)
        graph.return_value.compile.return_value = compiled_graph

        await workflow.run("test_goal")
        assert workflow.is_done

    mock_checkpoint = {
        "id": "checkpoint1",
        "channel_values": {
            "agent": "build_context",
            "conversation_history": {"build_context": [{}]},
        },
    }
    mock_git_lab_workflow_instance.aget_tuple = AsyncMock(
        return_value=CheckpointTuple(
            config={
                "configurable": {
                    "thread_id": "123",
                    "checkpoint_id": "checkpoint1",
                }
            },
            checkpoint=mock_checkpoint,  # type:ignore
            metadata={},
            parent_config={},
        )
    )

    # re-run the workflow
    async_iterator = AsyncIterator()

    with patch(
        "duo_workflow_service.workflows.software_development.workflow.StateGraph"
    ) as graph:
        compiled_graph = MagicMock()
        compiled_graph.astream.return_value = async_iterator
        compiled_graph.aget_state = AsyncMock(return_value=None)
        compiled_graph.aupdate_state = AsyncMock(return_value=None)
        graph.return_value.compile.return_value = compiled_graph

        await workflow.run("test_goal")
        assert workflow.is_done


def test_get_from_outbox():
    workflow = Workflow("123", {})
    workflow._outbox.put_nowait("test_item")
    item = workflow.get_from_outbox()
    assert item == "test_item"


def test_add_to_inbox():
    workflow = Workflow("123", {})
    event = contract_pb2.ClientEvent()
    workflow.add_to_inbox(event)
    assert workflow._inbox.qsize() == 1
    assert workflow._inbox.get_nowait() == event


@pytest.mark.asyncio
async def test_workflow_cleanup():
    workflow = Workflow("123", {})

    assert workflow._outbox.empty()
    assert workflow._inbox.empty()

    workflow._outbox.put_nowait("test_outbox_item_1")
    workflow._inbox.put_nowait("test_inbox_item_1")

    assert workflow._outbox.qsize() == 1
    assert workflow._inbox.qsize() == 1
    assert not workflow.is_done

    await workflow.cleanup("123")

    assert workflow.is_done
    assert workflow._outbox.qsize() == 0
    assert workflow._inbox.qsize() == 0
