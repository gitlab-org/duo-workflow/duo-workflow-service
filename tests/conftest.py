import pytest

from duo_workflow_service.entities.state import (
    MessageTypeEnum,
    Plan,
    WorkflowState,
    WorkflowStatusEnum,
)


@pytest.fixture(scope="function")
def workflow_state():
    return WorkflowState(
        plan=Plan(steps=[]),
        status=WorkflowStatusEnum.NOT_STARTED,
        conversation_history={},
        handover=[],
        last_human_input=None,
        ui_chat_log=[
            {
                "message_type": MessageTypeEnum.AGENT,
                "content": "This is a test message",
                "timestamp": "2025-01-08T12:00:00Z",
                "status": None,
                "correlation_id": None,
                "tool_info": None,
            }
        ],
    )
