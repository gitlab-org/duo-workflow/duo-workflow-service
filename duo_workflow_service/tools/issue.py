import json
from typing import Any, List, Optional, Type

from pydantic import BaseModel, Field

from duo_workflow_service.tools.duo_base_tool import DuoBaseTool

DESCRIPTION_CHARACTER_LIMIT = 1_048_576


class CreateIssueInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    title: str = Field(description="Title of the issue")
    description: Optional[str] = Field(
        description=f"The description of an issue. Limited to {DESCRIPTION_CHARACTER_LIMIT} characters."
    )
    labels: Optional[str] = Field(
        description="""Comma-separated label names to assign to the new issue.
                                  If a label does not already exist, this creates a new project label and assigns it to the issue."""
    )
    assignee_ids: Optional[list[int]] = Field(
        description="The IDs of the users to assign the issue to"
    )
    confidential: Optional[bool] = Field(
        description="Set to true to create a confidential issue. Default is false."
    )
    due_date: Optional[str] = Field(
        description="The due date. Date time string in the format YYYY-MM-DD, for example 2016-03-11."
    )
    issue_type: Optional[str] = Field(
        description="The type of issue. One of issue, incident, test_case or task. Default is issue."
    )


class CreateIssue(DuoBaseTool):
    name: str = "create_issue"
    description: str = "Create a new issue in a GitLab project"
    args_schema: Type[BaseModel] = CreateIssueInput  # type: ignore

    async def _arun(self, project_id: int, title: str, **kwargs: Any) -> str:
        data = {"title": title, **{k: v for k, v in kwargs.items() if v is not None}}

        try:
            response = await self.gitlab_client.apost(
                path=f"/api/v4/projects/{project_id}/issues",
                body=json.dumps(data),
            )
            return json.dumps({"created_issue": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: CreateIssueInput) -> str:
        return f"Create issue '{args.title}' in project {args.project_id}"


class ListIssuesInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    assignee_id: Optional[int] = Field(
        description="""Return issues assigned to the given user ID. It can't be used together with assignee_username.
                              None returns unassigned issues. Any returns issues with an assignee."""
    )
    assignee_username: Optional[List[str]] = Field(
        description="""Return issues assigned to the given username. This works like assignee_id but can't be used together with it. In GitLab CE,
                        assignee_username can only have one value. If there's more than one, an error will be returned."""
    )
    author_id: Optional[int] = Field(
        description="""Return issues created by the given user id.
                                     It can't be used together with author_username.
                                     Combine with scope=all or scope=assigned_to_me."""
    )
    author_username: Optional[str] = Field(
        description="""Return issues created by the given username.
                                           Similar to author_id and it can't be used together with author_id.
                                           Combine with scope=all or scope=assigned_to_me."""
    )
    confidential: Optional[bool] = Field(
        description="Filter confidential or public issues"
    )
    created_after: Optional[str] = Field(
        description="Return issues created on or after the given time. Expected in ISO 8601 date and time format (YYYY-MM-DDTHH:MM:SSZ)"
    )
    created_before: Optional[str] = Field(
        description="Return issues created on or before the given time. Expected in ISO 8601 date and time format (YYYY-MM-DDTHH:MM:SSZ)"
    )
    due_date: Optional[str] = Field(
        description="""Return issues that have no due date, are overdue, or whose due date is this week, this month, or between two weeks ago and next month.
        Accepts: 0 (no due date), any, today, tomorrow, overdue, week, month, next_month_and_previous_two_weeks."""
    )
    health_status: Optional[str] = Field(
        description="""Return issues with the specified health_status. None returns issues with no
            health status assigned, and Any returns issues with a health status assigned."""
    )
    issue_type: Optional[str] = Field(
        description="Filter to a given type of issue. One of issue, incident, test_case or task."
    )
    labels: Optional[str] = Field(
        description="""Comma-separated list of label names, issues must have all labels to be returned.
                                  None lists all issues with no labels. Any lists all issues with at least one label. Predefined names are case-insensitive."""
    )
    scope: Optional[str] = Field(
        description="Return issues for the given scope: created_by_me, assigned_to_me or all. Defaults to created_by_me."
    )
    search: Optional[str] = Field(
        description="Search issues against their title and description"
    )
    sort: Optional[str] = Field(
        description="Return issues sorted in asc or desc order. Default is desc."
    )
    state: Optional[str] = Field(
        description="Return all issues or just those that are opened or closed"
    )


class ListIssues(DuoBaseTool):
    name: str = "list_issues"
    description: str = "List issues in a GitLab project"
    args_schema: Type[BaseModel] = ListIssuesInput  # type: ignore

    async def _arun(self, project_id: int, **kwargs: Any) -> str:
        params = {k: v for k, v in kwargs.items() if v is not None}

        try:
            response = await self.gitlab_client.aget(
                path=f"/api/v4/projects/{project_id}/issues",
                params=params,
                parse_json=False,
            )
            return json.dumps({"issues": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: ListIssuesInput) -> str:
        return f"List issues in project {args.project_id}"


class GetIssueInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    issue_id: int = Field(description="The internal ID of the project issue")


class GetIssue(DuoBaseTool):
    name: str = "get_issue"
    description: str = "Get a single issue in a GitLab project"
    args_schema: Type[BaseModel] = GetIssueInput  # type: ignore

    async def _arun(self, project_id: int, issue_id: int) -> str:
        try:
            response = await self.gitlab_client.aget(
                path=f"/api/v4/projects/{project_id}/issues/{issue_id}",
                parse_json=False,
            )
            return json.dumps({"issue": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: GetIssueInput) -> str:
        return f"Read issue #{args.issue_id} in project {args.project_id}"


class UpdateIssueInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    issue_id: int = Field(description="The internal ID of the project's issue")
    title: Optional[str] = Field(description="Title of the issue")
    description: Optional[str] = Field(
        description=f"Description of the issue. Max character limit of {DESCRIPTION_CHARACTER_LIMIT} characters."
    )
    labels: Optional[str] = Field(description="Comma-separated list of label names")
    assignee_ids: Optional[list[int]] = Field(
        description="The ID of the users to assign the issue to. Set to `0` or provide an empty value to unassign all assignees."
    )
    confidential: Optional[bool] = Field(
        description="Set to true to make the issue confidential"
    )
    due_date: Optional[str] = Field(description="Date string in the format YYYY-MM-DD")
    state_event: Optional[str] = Field(
        description="The state event of an issue. To close the issue, use 'close', and to reopen it, use 'reopen'."
    )
    discussion_locked: Optional[bool] = Field(
        description="Flag indicating if the issue’s discussion is locked. If the discussion is locked only project members can add or edit comments."
    )


class UpdateIssue(DuoBaseTool):
    name: str = "update_issue"
    description: str = "Update an existing issue in a GitLab project"
    args_schema: Type[BaseModel] = UpdateIssueInput  # type: ignore

    async def _arun(self, project_id: int, issue_id: int, **kwargs: Any) -> str:
        data = {k: v for k, v in kwargs.items() if v is not None}

        try:
            response = await self.gitlab_client.aput(
                path=f"/api/v4/projects/{project_id}/issues/{issue_id}",
                body=json.dumps(data),
            )
            return json.dumps({"updated_issue": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: UpdateIssueInput) -> str:
        return f"Update issue #{args.issue_id} in project {args.project_id}"


class CreateIssueNoteInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    issue_id: int = Field(description="The internal ID of the project issue")
    body: str = Field(
        description="The content of the note. Limited to 1,000,000 characters."
    )


class CreateIssueNote(DuoBaseTool):
    name: str = "create_issue_note"
    description: str = "Create a new note (comment) on a GitLab issue"
    args_schema: Type[BaseModel] = CreateIssueNoteInput  # type: ignore

    async def _arun(self, project_id: int, issue_id: int, body: str) -> str:
        try:
            response = await self.gitlab_client.apost(
                path=f"/api/v4/projects/{project_id}/issues/{issue_id}/notes",
                body=json.dumps(
                    {
                        "body": body,
                    }
                ),
            )
            return json.dumps({"status": "success", "body": body, "response": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: CreateIssueNoteInput) -> str:
        return f"Add comment to issue #{args.issue_id} in project {args.project_id}"


class ListIssueNotesInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    issue_id: int = Field(description="The internal ID of the project issue")
    sort: Optional[str] = Field(
        description="Return issue notes sorted in asc or desc order. Default is desc"
    )
    order_by: Optional[str] = Field(
        description="Return issue notes ordered by created_at or updated_at fields. Default is created_at"
    )


class ListIssueNotes(DuoBaseTool):
    name: str = "list_issue_notes"
    description: str = "Get a list of all notes (comments) for a specific issue"
    args_schema: Type[BaseModel] = ListIssueNotesInput  # type: ignore

    async def _arun(self, project_id: int, issue_id: int, **kwargs: Any) -> str:
        params = {k: v for k, v in kwargs.items() if v is not None}

        try:
            response = await self.gitlab_client.aget(
                path=f"/api/v4/projects/{project_id}/issues/{issue_id}/notes",
                params=params,
                parse_json=False,
            )
            return json.dumps({"notes": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: ListIssueNotesInput) -> str:
        return f"Read comments on issue #{args.issue_id} in project {args.project_id}"


class GetIssueNoteInput(BaseModel):
    project_id: int = Field(description="Id of the project")
    issue_id: int = Field(description="The internal ID of the project issue")
    note_id: int = Field(description="The ID of the note")


class GetIssueNote(DuoBaseTool):
    name: str = "get_issue_note"
    description: str = "Get a single note (comment) from a specific issue"
    args_schema: Type[BaseModel] = GetIssueNoteInput  # type: ignore

    async def _arun(self, project_id: int, issue_id: int, note_id: int) -> str:
        try:
            response = await self.gitlab_client.aget(
                path=f"/api/v4/projects/{project_id}/issues/{issue_id}/notes/{note_id}",
                parse_json=False,
            )
            return json.dumps({"note": response})
        except Exception as e:
            return json.dumps({"error": str(e)})

    def format_display_message(self, args: GetIssueNoteInput) -> str:
        return f"Read comment #{args.note_id} on issue #{args.issue_id} in project {args.project_id}"
