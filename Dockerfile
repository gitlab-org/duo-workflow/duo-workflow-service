FROM python:3.11.11-slim AS base-image

ENV PYTHONUNBUFFERED=1 \
  PIP_NO_CACHE_DIR=1 \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  POETRY_VERSION=1.8.5 \
  CLOUD_CONNECTOR_SERVICE_NAME=${CLOUD_CONNECTOR_SERVICE_NAME}

WORKDIR /app

COPY poetry.lock pyproject.toml ./
RUN pip install "poetry==$POETRY_VERSION"

# Install all dependencies into /opt/venv
# so that we can copy these resources between
# build stages
RUN poetry config virtualenvs.path /opt/venv

##
## Intermediate image contains build-essential for installing
## google-cloud-profiler's dependencies
##
FROM base-image AS install-image

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    && rm -rf /var/lib/apt/lists/*

RUN poetry install --no-interaction --no-ansi --no-cache --no-root --only main

##
## Final image copies dependencies from install-image
##
FROM base-image AS final

# Cloud Run first-generation execution environment overrides HOME to be /home
# (see https://cloud.google.com/run/docs/known-issues#home) so we override
# the POETRY_CONFIG_DIR setting so it knows where to find the config file
ENV POETRY_CONFIG_DIR=/root/.config/pypoetry

COPY --from=install-image /opt/venv /opt/venv

COPY duo_workflow_service/ duo_workflow_service/
COPY contract/ contract/

# Opening a default port for running it as a service container in CI/CD pipelines.
EXPOSE 50052

COPY ./scripts/run.sh .
CMD ["./run.sh"]
