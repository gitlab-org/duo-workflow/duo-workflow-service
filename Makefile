ROOT_DIR := $(shell pwd)
TESTS_DIR := ${ROOT_DIR}/tests
DUO_WORKFLOW_SERVICE_DIR := ${ROOT_DIR}/duo_workflow_service

LINT_WORKING_DIR ?= ${DUO_WORKFLOW_SERVICE_DIR} \
	${TESTS_DIR}

# grpc

PROTOC_VERSION := 27.3
PROTOC_GEN_GO_VERSION := v1.35.1
PROTOC_GEN_GO_GRPC_VERSION := v1.5.1

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
    OS := linux
else ifeq ($(UNAME_S),Darwin)
    OS := osx
else
    $(error Unsupported operating system: $(UNAME_S))
endif

UNAME_M := $(shell uname -m)
ifeq ($(UNAME_M),x86_64)
    ARCH := x86_64
else ifeq ($(UNAME_M),arm64)
    ARCH := aarch_64
else
    $(error Unsupported architecture: $(UNAME_M))
endif

.PHONY: gen-proto
gen-proto: gen-proto-python gen-proto-go gen-proto-ruby

.PHONY: gen-proto-python
gen-proto-python: install-test-deps
	poetry run python -m grpc_tools.protoc \
		-I ./ \
		--python_out=./ \
		--pyi_out=./ \
		--grpc_python_out=./ \
		./contract/contract.proto

.PHONY: gen-proto-ruby
gen-proto-ruby:
	(cd clients/ruby; bundle install)
	grpc_tools_ruby_protoc -I contract --ruby_out=clients/ruby/lib/proto --grpc_out=clients/ruby/lib/proto contract/contract.proto
	sed -i.bak "s/require 'contract_pb'/require_relative 'contract_pb'/" clients/ruby/lib/proto/contract_services_pb.rb
	rm clients/ruby/lib/proto/contract_services_pb.rb.bak

.PHONY: gen-proto-go
gen-proto-go: gen-proto-go-install bin/protoc
	bin/protoc --go_out=clients/gopb --go_opt=paths=source_relative \
		--go-grpc_out=clients/gopb --go-grpc_opt=paths=source_relative \
		./contract/contract.proto

.PHONY: gen-proto-go-install
gen-proto-go-install:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@$(PROTOC_GEN_GO_VERSION)
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@$(PROTOC_GEN_GO_GRPC_VERSION)

tmp/protoc-${PROTOC_VERSION}/bin/protoc:
	wget https://github.com/protocolbuffers/protobuf/releases/download/v$(PROTOC_VERSION)/protoc-$(PROTOC_VERSION)-$(OS)-$(ARCH).zip -O tmp/protoc-$(PROTOC_VERSION)-$(OS)-$(ARCH).zip
	unzip tmp/protoc-${PROTOC_VERSION}-$(OS)-$(ARCH).zip "bin/protoc" -d tmp/protoc-${PROTOC_VERSION}

bin/protoc: tmp/protoc-${PROTOC_VERSION}/bin/protoc
	cp tmp/protoc-${PROTOC_VERSION}/bin/protoc bin/protoc

# test

.PHONY: install-test-deps
install-test-deps:
	@echo "Installing test dependencies..."
	@poetry install --with test

.PHONY: test
test: install-test-deps
	@echo "Running tests..."
	@poetry run pytest

.PHONY: test-watch
test-watch: install-test-deps
	@echo "Running tests in watch mode..."
	@poetry run ptw .

.PHONY: test-coverage
test-coverage: install-test-deps
	@echo "Running tests with coverage..."
	@poetry run pytest --cov=duo_workflow_service --cov=lints --cov-report term --cov-report html

.PHONY: test-coverage-ci
test-coverage-ci: install-test-deps
	@echo "Running tests with coverage on CI..."
	@poetry run pytest --cov=duo_workflow_service --cov=lints --cov-report term --cov-report xml:.test-reports/coverage.xml --junitxml=".test-reports/tests.xml"

# lint

.PHONY: install-lint-deps
install-lint-deps:
	@echo "Installing lint dependencies..."
	@poetry install --only lint

.PHONY: black
black: install-lint-deps
	@echo "Running black format..."
	@poetry run black ${LINT_WORKING_DIR}

.PHONY: isort
isort: install-lint-deps
	@echo "Running isort format..."
	@poetry run isort ${LINT_WORKING_DIR}

.PHONY: format
format: black isort

.PHONY: lint
lint: flake8 check-black check-isort check-pylint check-mypy

.PHONY: flake8
flake8: install-lint-deps
	@echo "Running flake8..."
	@poetry run flake8 ${LINT_WORKING_DIR}

.PHONY: check-black
check-black: install-lint-deps
	@echo "Running black check..."
	@poetry run black --check ${LINT_WORKING_DIR}

.PHONY: check-isort
check-isort: install-lint-deps
	@echo "Running isort check..."
	@poetry run isort --check-only ${LINT_WORKING_DIR}

.PHONY: check-pylint
check-pylint: install-lint-deps install-test-deps
	@echo "Running pylint check..."
	@poetry run pylint ${LINT_WORKING_DIR}

.PHONY: check-mypy
check-mypy: install-lint-deps
ifeq ($(TODO),true)
	@echo "Running mypy check todo..."
	@poetry run mypy ${LINT_WORKING_DIR}
else
	@echo "Running mypy check..."
	@poetry run mypy ${LINT_WORKING_DIR} ${MYPY_LINT_TODO_DIR} --exclude "scripts/vendor/*"
endif
